﻿using UnityEngine;
using System.Collections;

public class Woman : MonoBehaviour {

    [SerializeField]
	public float health;
	
	// Update is called once per frame
	void Update ()
    {
	    if (health < 0)
        {
            // Game Over
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        GetComponent<AICharacter>().RandomizeNewPosition();
    }
}
