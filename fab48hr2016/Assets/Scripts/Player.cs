﻿using UnityEngine;
using Rewired;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject pickupAnchor;

    [SerializeField]
    private float aimRotationAngle;

    [SerializeField]
    private float throwPower;

    private CharacterController cc;

    private GameObject attachedObject = null;

    private Vector3 cameraOffset = Vector3.zero;

    private Rewired.Player rewiredPlayer;

    private bool isCollidingWithObject = false;

    [SerializeField]
    private GameObject raycastPoint;

    [SerializeField]
    private TRex trex;

    private Animator animator;

	public AudioSource audioSource;
	public AudioClip clip, clip2;

    [SerializeField]
    private Explosion explosion;


	// Use this for initialization
	void Start ()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
        cc = GetComponent<CharacterController>();
        cameraOffset = Camera.main.transform.position;
        animator = GetComponentInChildren<Animator>();
		Invoke ("LifeBegins",0);
    }

	void LifeBegins()
	{
		Invoke ("LifeFinds", Random.Range(10,20));
	}

	void LifeFinds()
	{
		float poop = Random.Range (1, 2);
		if (poop == 1) {
			audioSource.PlayOneShot (clip,1);
		}
		if (poop == 2) {
			audioSource.PlayOneShot (clip2,1);
		}
		audioSource.PlayOneShot (clip,1);
		Invoke ("LifeBegins",0);
	}

	// Update is called once per frame
	void Update ()
    {
        // Handle Collisions
        HandleCollisions();

        // If T-Rex is down and out. Finish it off
        if (trex.isDownAndOut())
            FinishTRex();

        // Check for Fire
        if (rewiredPlayer.GetButtonUp("Throw"))
        {
			if (attachedObject != null)
            	animator.SetTrigger("Throw");
            //ThrowObject();
        }

        Vector3 movement = MovePlayer();

        cc.Move(movement);

        // Move the camera
        Camera.main.transform.position = transform.position + cameraOffset;
	}

    void HandleCollisions()
    {
        if (rewiredPlayer.GetButtonDown("Pickup"))
        {
            // Run a raycast
            Vector3 fwd = raycastPoint.transform.TransformDirection(Vector3.forward);

            Ray ray = new Ray(raycastPoint.transform.position, fwd);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 5))
            {
                if (attachedObject != null)
                    return;

				if (hit.collider.gameObject.tag != "PickupObject")
					return;

                // Pickup Animation
                animator.SetTrigger("Pickup");

                // Pickup the object
                attachedObject = hit.collider.gameObject;

                //PickupObject(hit.collider.gameObject);
            }
        }
    }

    public void PickupObject()
    {
        // Rotate the pickup anchor
        Vector3 objRotation = new Vector3(-(Mathf.Abs(aimRotationAngle)), 0, 0);

        attachedObject.transform.SetParent(pickupAnchor.transform);
        attachedObject.transform.position = pickupAnchor.transform.position;
        attachedObject.transform.rotation = pickupAnchor.transform.rotation;

        attachedObject.GetComponent<MeshCollider>().enabled = false;
        attachedObject.GetComponent<MeshCollider>().isTrigger = false;
        Destroy(attachedObject.GetComponent<Rigidbody>());
    }

    public void ThrowObject()
    {
        // Check if we are holding an object
        if (attachedObject != null)
        {
            // Re-enable the box collider and add a rigidbody
            attachedObject.GetComponent<MeshCollider>().enabled = true;
            attachedObject.AddComponent<Rigidbody>();

            // Rotate the object for optimum aim - Not making any different in the applied force
            Vector3 objRotation = new Vector3(-(Mathf.Abs(aimRotationAngle)), 0, 0);
            attachedObject.transform.rotation = Quaternion.Euler(objRotation);

            // Face the TRex
            transform.LookAt(trex.transform);

            // Get the forward vector
            Vector3 forward = transform.TransformDirection(Vector3.forward);

            // Throw the object
            attachedObject.GetComponent<Rigidbody>().AddForce(forward * throwPower);

            attachedObject.GetComponent<PickupObject>().GettingThrown();

            // Remove parent
            attachedObject.transform.SetParent(null);
            attachedObject = null;
        }
    }

    Vector3 MovePlayer()
    {
        // Move Character
        Vector3 movement = new Vector3();
        movement.x = rewiredPlayer.GetAxis("Horizontal");
        movement.z = rewiredPlayer.GetAxis("Vertical");

        if (movement != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(movement);
        }

        return movement;
    }

    void FinishTRex()
    {
        if (Vector3.Distance(transform.position, trex.transform.position) < 75.0f)
        {
            if (rewiredPlayer.GetButtonUp("Flip"))
            {
                animator.SetTrigger("Flip");
                //trex.gameObject.AddComponent<Rigidbody>();

                Vector3 forward = transform.TransformDirection(Vector3.forward);

                trex.Flipped();

                //trex.GetComponent<Rigidbody>().AddForce(1500f * forward, ForceMode.Impulse);


                // Set the rigidbody settings
                //Rigidbody rb = trex.GetComponent<Rigidbody>();
                //rb.mass = 1;
                //rb.drag = 0;
                //rb.angularDrag = 0.05f;
                //rb.useGravity = true;
                //rb.AddForce(new Vector3(0, 1, 0) * 1500f, ForceMode.Impulse);
                //rb.AddExplosionForce(1500f, trex.transform.position, 1500f, 1500f, ForceMode.Impulse);

                //Instantiate(explosion, trex.transform.position, trex.transform.rotation);

                

                Application.LoadLevel (4);
            }
        }
    }
}
