﻿using UnityEngine;
using System.Collections;

public class TRexEat : MonoBehaviour
{
    [SerializeField]
    private TRex trex;

    [SerializeField]
    private float eatDelay;

    private bool justEaten = false;

    private float delayTimer = 0.0f;

    void Update()
    {
        if (justEaten)
        {
            delayTimer += Time.deltaTime;
        }

        if (delayTimer > eatDelay)
        {
            justEaten = false;
            trex.EatingTime(false);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "JeffGodlblum")
        {
            // Jeff Game Over
            Time.timeScale = 0;
            justEaten = true;
            trex.EatingTime(true);
        }
        else if (col.gameObject.name == "Big Tits")
        {
            // Get T-Rex Damage
            float damage = trex.GetDamage();

            // Woman takes damage
            col.gameObject.GetComponent<Woman>().TakeDamage(damage);

            justEaten = true;
            trex.EatingTime(true);
        }
        else
        {
            return;
        }
    }
}
