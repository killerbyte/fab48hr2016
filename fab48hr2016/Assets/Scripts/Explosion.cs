﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(Collider))]
public class Explosion : MonoBehaviour
{
    private Rigidbody _rigidbody;

	// Use this for initialization
	void Start ()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.AddExplosionForce(1500f, transform.position, 1500f, 1500f, ForceMode.Impulse);
        Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}
}
