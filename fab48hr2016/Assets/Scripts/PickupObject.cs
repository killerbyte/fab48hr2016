﻿using UnityEngine;
using System.Collections;

public class PickupObject : MonoBehaviour
{
    [SerializeField]
    private float damage;
    private bool isThrown = false;

    public float GetDamage()
    {
        return damage;
    }

    public void GettingThrown()
    {
        isThrown = true;
    }

    public bool hasThrown()
    {
        return isThrown;
    }
}
