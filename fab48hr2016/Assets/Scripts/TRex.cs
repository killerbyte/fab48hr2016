﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TRex : MonoBehaviour
{
    [SerializeField]
	public float health;
    private float maxHealth;

    [SerializeField]
    private float damage;

    private Animator animator;

    [SerializeField]
    private Player player;

    [SerializeField]
    private Woman woman;

    private bool isStunned;

    [SerializeField]
    private float dangerDistance;

    private NavMeshAgent nav;

    [SerializeField]
    private float stunTime;

    private float elapsedTime;
    private float stunnedTime;

    // Damage flags
    private bool firstStun = false;
    private bool secondStun = false;
    private bool downAndOut = false;

    private bool justEaten = false;

    [SerializeField]
    private float navigationRecalcuationTime;

    public bool isFlipped = false;

    // Use this for initialization
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();

        animator = GetComponentInChildren<Animator>();

        maxHealth = health;
    }

    // Update is called once per frame
    void Update()
    {

        if (isFlipped)
        {
            transform.Translate(Vector3.up);
            return;
        }

        // Check health
        if (health < maxHealth * 0.75 && !firstStun) // First stun
        {
            Stun();
            firstStun = true;
        }
        else if (health < maxHealth * 0.5 && firstStun && !secondStun) // Second stun
        {
            Stun();
            secondStun = true;
        }
        else if (health < maxHealth * 0.25 && secondStun) // Finish Him!!
        {
            // Go down for the final count
            animator.SetBool("isDead", true);
            nav.Stop();
            downAndOut = true;
        }

        if (!downAndOut)
        {
            CheckTargetDistance(player.gameObject);
            CheckTargetDistance(woman.gameObject);

            if (elapsedTime > navigationRecalcuationTime)
                nav.SetDestination(woman.transform.position);

            if (isStunned)
            {
                stunnedTime -= Time.deltaTime;
                CheckStunStatus();
            }

            Debug.Log(justEaten);
        }

        elapsedTime += Time.deltaTime;
    }

    void OnCollisionEnter(Collision col)
    {
        // Take damage. Can be stunned twice before Jeff can finish him off.
        // Stun = 3

        if (col.gameObject.tag == "PickupObject")
        {
            // Check whether the object has been thrown
            if (!col.gameObject.GetComponent<PickupObject>().hasThrown())
            {
                return;
            }

            //animator.SetTrigger("Damaged");

            health -= col.gameObject.GetComponent<PickupObject>().GetDamage();
			Invoke ("SliderNumb",0);
            Destroy(col.gameObject);
        }
    }

    void CheckTargetDistance(GameObject go)
    {
        if (isStunned)
            return;

        if (justEaten)
            return;

        if (Vector3.Distance(transform.position, go.transform.position) < dangerDistance)
        {
            transform.LookAt(go.transform);
            animator.SetTrigger("Eat");
        }
    }

    void Stun()
    {
        isStunned = true;
        nav.Stop();
        stunnedTime = stunTime;
        animator.SetBool("Stunned", true);
    }

    void CheckStunStatus()
    {
        if (stunnedTime < 0)
        {
            isStunned = false;
            nav.Resume();
            animator.SetBool("Stunned", false);
        }
    }

    public float GetDamage()
    {
        return damage;
    }

    public void EatingTime(bool value)
    {
        justEaten = value;
    }

    public bool isDownAndOut()
    {
        return downAndOut;
    }

    public void Flipped()
    {
        isFlipped = true;
        nav.Stop();
    }
		

}
