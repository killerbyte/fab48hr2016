﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(NavMeshAgent))]
public class AICharacter : MonoBehaviour
{
    [SerializeField]
    private GameObject waypointList;

    [SerializeField]
    private int currentWaypoint;

    private NavMeshAgent nav;

    private float elapsedTime;

    [SerializeField]
    private float navigationRecalcuationTime;

    [SerializeField]
    private float navigationRecalculationDistance;

	public AudioSource audioSources;
	public AudioClip scream;


	// Use this for initialization
	void Start ()
    {
        RandomizeNewPosition();
		if (this.gameObject.name == "Big Tits") {
			Invoke ("Scream", 0);
		}
	}

	void Scream()
	{
		Invoke ("ScreamBitch", Random.Range(5,20));
	}
	void ScreamBitch()
	{
		audioSources.PlayOneShot (scream, 1);
		Invoke ("Scream",0);
	}

	// Update is called once per frame
	void Update ()
    {
        if (currentWaypoint == waypointList.transform.childCount)
            currentWaypoint = 0;

        Vector3 waypointPos = waypointList.transform.GetChild(currentWaypoint).position;

        if (elapsedTime > navigationRecalcuationTime)
            nav.SetDestination(waypointPos);

        if (Vector3.Distance(transform.position, waypointPos) < navigationRecalculationDistance)
            currentWaypoint = Random.Range(0, waypointList.transform.childCount);

        elapsedTime += Time.deltaTime;
	}

    public void RandomizeNewPosition()
    {
        currentWaypoint = Random.Range(0, waypointList.transform.childCount);

        nav = GetComponent<NavMeshAgent>();
    }
}
