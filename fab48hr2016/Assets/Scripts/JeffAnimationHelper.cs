﻿using UnityEngine;
using System.Collections;

public class JeffAnimationHelper : MonoBehaviour {

    void PickupObject()
    {
        transform.parent.GetComponent<Player>().PickupObject();
    }

    void ThrowObject()
    {
        transform.parent.GetComponent<Player>().ThrowObject();
    }
}
