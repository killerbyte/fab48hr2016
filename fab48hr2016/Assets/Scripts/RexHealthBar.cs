﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RexHealthBar : MonoBehaviour {
	public GameObject tRexObj;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		float trexHealth = tRexObj.GetComponent<TRex> ().health;
		GetComponent<Slider> ().value = trexHealth;

	
	}
}
