﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class JeffMenu : MonoBehaviour {

	public Animator jeffMenuAnimator, fontAnimator, buttonAnimator;
	// Use this for initialization
	void Start () {
		Invoke("AnimateJeff", 1.5f);
		Invoke("FontAnimate", 4f);
	}
	
	void AnimateJeff () {
		jeffMenuAnimator.SetInteger ("Stretch",1);
		Invoke("Buff", 0.5f);
	}
	void Buff()
	{
		jeffMenuAnimator.SetInteger ("Stretch",0);
		Invoke ("AnimateJeff",10);
	}

	void FontAnimate()
	{
		fontAnimator.SetInteger ("FontGo",1);
		buttonAnimator.SetInteger ("ButtonGo",1);
	}


	public void LoadLevel()
	{
		Application.LoadLevel (1);
	}

}
