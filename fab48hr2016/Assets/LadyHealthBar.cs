﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LadyHealthBar : MonoBehaviour {
	public GameObject ladyObj;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		float trexHealth = ladyObj.GetComponent<Woman> ().health;
		GetComponent<Slider> ().value = trexHealth;
		if (trexHealth <= 0) {
			Application.LoadLevel (3);
		}
	}
}
