﻿using UnityEngine;
using Rewired;
using System.Collections;

public class MoviePlayer : MonoBehaviour {
	public MovieTexture movTexture;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().material.mainTexture = movTexture;
		movTexture.Play();
		Invoke ("GameTime",41);
	}

	void GameTime()
	{
		Application.LoadLevel (2);
	}


	// Update is called once per frame
	void Update () {
		if (ReInput.players.GetPlayer(0).GetButtonUp("SkipCutscene"))
		{
			movTexture.Stop();
			GameTime();
		}
	}
}
